provider "google" {
  region      = "${var.region}"
  project     = "${var.project}"
  zone        = "${var.zone}"
  credentials = "${file("${var.credentials_file_path}")}"
}

terraform {
  backend "gcs" {
    bucket      = "my-bucket"
    prefix      = "terraform/state/gce/gke"
    credentials = "${file("${var.credentials_file_path}")}"
  }
}