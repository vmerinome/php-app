resource "google_container_cluster" "pre" {
  name     = "pre"
  location = "europe-west1"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count = 1

  # Setting an empty username and password explicitly disables basic auth
  master_auth {
    username = ""
    password = ""
  }
}

resource "google_container_node_pool" "php-nodepool" {
  name       = "php"
  location   = "europe-west1"
  cluster    = "${google_container_cluster.pre.name}"
  node_count = 1

  node_config {
    machine_type = "g1-small"

    disk_size_gb = 15

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/service.management.readonly",
      "https://www.googleapis.com/auth/devstorage.read_only"
    ]
  }
}
