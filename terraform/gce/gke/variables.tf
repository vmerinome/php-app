#Common

variable "credentials_file_path" {
  description = "Location of the credentials file to use"
  default     = "/path/to/my/credentialfile.json"
}

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-b"
}

variable "project" {
  default = "my-gcp-project-id"
}